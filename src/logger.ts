const logNumbers = {
  debug: 1,
  info: 2,
  error: 3,
  silent: 4,
} as const;

export type LogType = keyof typeof logNumbers;

interface Log {
  type: LogType;
  message: string;
  date: Date;
}

type GoodLogPart = string | number | undefined;
type LogPart = GoodLogPart | Error;

export class Logger {
  static readonly LOG_TYPES = Object.keys(logNumbers) as LogType[];

  private readonly queue: Log[] = [];
  private queueIndex = 0;
  private readonly minLogLevel: number;

  constructor(minLogLevel: LogType, readonly maxSize: number) {
    this.assertLogType(minLogLevel);
    this.minLogLevel = logNumbers[minLogLevel];
  }

  private log(type: LogType, parts: LogPart[]) {
    this.queue[this.queueIndex] = {
      type,
      message: parts.join(" "),
      date: new Date(),
    };

    if (this.queueIndex === this.maxSize) {
      this.queueIndex = 0;
    } else {
      this.queueIndex++;
    }
  }

  debug(...parts: GoodLogPart[]) {
    if (this.minLogLevel <= logNumbers.debug) {
      this.log("debug", parts);
      console.debug(...parts);
    }
  }

  info(...parts: GoodLogPart[]) {
    if (this.minLogLevel <= logNumbers.info) {
      this.log("info", parts);
      console.log(...parts);
    }
  }

  error(...parts: LogPart[]) {
    if (this.minLogLevel <= logNumbers.error) {
      this.log("error", parts);
      console.error("error", ...parts);
    }
  }

  getLogs(minLogLevel: LogType = "debug") {
    this.assertLogType(minLogLevel);

    const minLogLevelNumber = logNumbers[minLogLevel];

    return [
      ...this.queue.slice(this.queueIndex),
      ...this.queue.slice(0, this.queueIndex),
    ].filter(x => minLogLevelNumber <= logNumbers[x.type]);
  }

  private assertLogType(logType: LogType) {
    if (!Logger.LOG_TYPES.includes(logType)) {
      const logTypes = Logger.LOG_TYPES.join(", ");
      throw new Error(
        `Unknown log type '${logType}'. Valid values: ${logTypes}`,
      );
    }
  }
}
